from flask import render_template, request
from flask import Flask
import conversionnew

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    if request.method == "GET":
        return render_template('index.html')


@app.route('/length/', methods=['POST', 'GET'])
def length():
    if request.method == "GET":
        return render_template('length.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        value_to_convert = float(request.form.get("value_to_convert"))
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversionnew.Converter(
            "length", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_output returns 2 things: the output, and the warnings that came with it.
        warnings = converter.return_output()[1]

        return render_template('length.html', output=output, warnings=warnings, method=request.method)


@app.route('/mass/', methods=['POST', 'GET'])
def mass():
    if request.method == "GET":
        return render_template('mass.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        value_to_convert = float(request.form.get("value_to_convert"))
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversionnew.Converter(
            "mass", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('mass.html', output=output, warnings=warnings, method=request.method)


@app.route('/volume/', methods=['POST', 'GET'])
def volume():
    if request.method == "GET":
        return render_template('volume.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        value_to_convert = float(request.form.get("value_to_convert"))
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversionnew.Converter(
            "volume", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('volume.html', output=output, warnings=warnings, method=request.method)


@app.route('/temperature/', methods=['POST', 'GET'])
def temperature():
    if request.method == "GET":
        return render_template('temperature.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        value_to_convert = float(request.form.get("value_to_convert"))
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversionnew.Converter(
            "temperature", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('temperature.html', output=output, warnings=warnings, method=request.method)


@app.route('/time/', methods=['POST', 'GET'])
def time():
    if request.method == "GET":
        return render_template('time.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        value_to_convert = float(request.form.get("value_to_convert"))
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversionnew.Converter(
            "time", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('time.html', output=output, warnings=warnings, method=request.method)


if __name__ == '__main__':
    app.run(debug=True)
