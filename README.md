# Unit Converter


This is an application that uses Python's Flask library to create a unit converter.
No library is used for unit conversion (i.e. all unit conversions/algorithms were created by me).

This unit converter currently has these available measurement types: length, mass, temperature, time, and volume. This application has a very large sum of units, unlike most online unit converters, which typically only have a few basic units.

The only external import (the only library) within the program is Flask, which needs to be manually installed, most of the time.

After that, just run `python3 app.py` if your CWD is within the top-level directory of this program. After that, a local server will start, with a local IP shown in the console.

Within this repository is also a single file called `conversionTerminal.py`. This file is the *terminal application* variant of this app, meaning it is the headless/no GUI version. This file can also be run with `python3`.

## Simple Video Demo

[Click here for a simple demo video](https://youtu.be/O13qA8dE-_U?si=1PfC_0c3DNp6175j)


## How it Works

This program handles a wide variety of units, spanning metric, imperial, and US volume measurement systems. It currently supports 5 measurement types:
- Length
- Mass
- Volume
- Temperature
- Time

### Under the Hood: Multiple Dictionaries for Precision

To achieve accurate conversions, the program uses a base-oriented approach to data storage and representation. Instead of having a single dictionary for each measurement type (e.g. a length dictionary and a mass dictionary), it utilizes multiple dictionaries with different bases. Here's why:

- **Base Units and Accuracy:** Each dictionary has a designated "base unit" with a value of 1. The values of other units are expressed relative to this     base. 
    - This structure ensures maximum accuracy when converting between units within the same system.
- **System-Specific Dictionaries:** For example, there are separate dictionaries for metric length units (using meters as the base) and imperial length units (using inches as the base). This separation allows for precise conversions within each system.

### Choosing the Right Base

The program automatically selects the appropriate dictionary based on the units involved in the conversion. For simplicity, the following list only talks about metric and imperial systems (not US systems), because this is just meant to demonstrate understanding of the program:

- **Metric to Metric:** If both units are metric, the metric dictionary is used.
- **Imperial to Imperial:** If both units are imperial, the imperial dictionary is used.
- **Metric to Imperial (or vice versa):** In these cases, the metric base is used as a default, offering the best possible accuracy for conversions between systems.

#### Example: Metric vs. Imperial Base for Length

- **Metric Base (meter as base):** - kilometer = 1000 - meter = 1 - decimeter = 0.1 - centimeter = 0.01 - inch = 0.0254 - foot = 0.3048
- **Imperial Base (inch as base):** - kilometer = 39370.1 - meter = 39.3701 - decimeter = 3.93701 - centimeter = 0.393701 - inch = 1 - foot = 12

This approach ensures optimal accuracy for all conversions, regardless of the measurement systems involved.

