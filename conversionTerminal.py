# NOTE: The only difference between the different converters within this program is the base unit. So if you were converting imperials, it would be best to have a compatible base instead of converting the imperials from a metric base unit.
# Program starts out with metric just to see all of the units (metric will always have the most since imperial dicts only contain imperials, while metrics contain both)
# Later on, the program decides which system to use based on the units provided by the user
# Imperial+Imperial = Imperial, Metric+Metric = Metric, Imperial+Metric = Metric
# NOTE: For volume, the overriding order is Metric, US, Imperial (us has different volume units, thats why there are 3 this time)
#############################
# For all unit lists:
# 0: Conversion-Compatible measurement system (not actual measurement system, e.g. nautical mile is labeled as metric since it is compatible with metric and not imperial, although in reality it is imperial)
# 1: Abbreviation
# 2: Conversion factor
#############################

#############################
# For all unit checker functions within each converter class:
######################
# 0: Exists in dict  #
# 1: Is abbreviation #
######################
#############################

# Dicts containing unit data
# Metric ones are used by default in Data class for valdiation, but no calculations
# That's why they aren't called *_METRIC

# Abbreviations in the master (metric) dictionaries are used for validation
# Abbreviations in any other dictionary are used for displaying at the end.


LENGTH = {
    "mile": ['imperial', 'mi', 1609.344],
    "yard": ['imperial', 'yd', 0.9144],
    "foot": ['imperial', 'ft', 0.3048],
    "inch": ['imperial', 'in', 0.0254],
    "nautical mile": ['metric', 'NM', 1852, "NOTE: This is actually imperial, but has exact conversions."],
    "yottameter": ['metric', 'Ym',   1_000_000_000_000_000_000_000_000],
    "zettameter": ['metric', 'Zm',   1000000000000000000000],
    "exameter": ['metric', 'Em',     1000000000000000000],
    "pecameter": ['metric', 'Pm',     1000000000000000],
    "terameter": ['metric', 'Tm',     1000000000000],
    "gigameter": ['metric', 'Gm',     1000000000],
    "megameter": ['metric', 'Mm',     1000000],
    "kilometer": ['metric', 'km',     1000],
    "hectometer": ['metric', 'hm',    100],
    "decameter": ['metric', 'dam',    10],
    "meter": ['metric', 'm',          1],
    "decimeter": ['metric', 'dm',     0.1],
    "centimeter": ['metric', 'cm',    0.01],
    "millimeter": ['metric', 'mm',    0.001],
    "micrometer": ['metric', 'µm',    0.000001],
    "nanometer": ['metric', 'nm',     0.000000001],
    "picometer": ['metric', 'pm',     0.000000000001],
    "femtometer": ['metric', 'fm',    0.000000000000001],
    "attometer": ['metric', 'am',     0.000000000000000001],
    "zeptometer": ['metric', 'zm',    0.000000000000000000001],
    "yactometer": ['metric', 'ym',    0.000000000000000000000001],
    "rontometer": ['metric', 'rm',    0.000000000000000000000000001],
    "quectometer": ['metric', 'qm',   0.000000000000000000000000000001],
    "planck length": ['metric', 'ℓP', 0.000000000000000000000000000000000016],
}
LENGTH_IMPERIAL = {
    "nautical mile": ['metric', 'NM', 72913.385826772, "NOTE: This is actually imperial, but has exact conversions."],
    "mile": ['imperial', 'mi', 63360],
    "yard": ['imperial', 'yd', 36],
    "foot": ['imperial', 'ft', 12],
    "inch": ['imperial', 'in', 1],
}
MASS = {
    "yottatonne": ['metric', 'Yt',  1000000000000000000000000000000],
    "zettatonne": ['metric', 'Zt',  1000000000000000000000000000],
    # same as yottagram
    "exatonne": ['metric', 'Et',    1000000000000000000000000],
    "yottogram": ['metric', 'Yg',   1000000000000000000000000],
    # same as zettagram
    "petatonne": ['metric', 'Pt',   1000000000000000000000],
    "zettagram": ['metric', 'Zg',   1000000000000000000000],
    # same as exagram
    "teratonne": ['metric', 'Tt',   1000000000000000000],
    "exagram": ['metric', 'Yg',     1000000000000000000],
    # same as pentagram
    "gigatonne": ['metric', 'Gt',   1000000000000000],
    "petagram": ['metric', 'Pg',    1000000000000000],
    # same as teragram
    "megatonne": ['metric', 'Mt',   1000000000000],
    "teragram": ['metric', 'Tg',    1000000000000],
    # same as gigagram
    "kilotonne": ['metric', 'kt',   1000000000],
    "gigagram": ['metric', 'Gg',    1000000000],
    # same as megagram
    "tonne": ['metric', 't',        1000000],
    "megagram": ['metric', 'Mg',    1000000],
    "kilogram": ['metric', 'kg',    1000],
    "hectogram": ['metric', 'hg',   100],
    "gram": ['metric', 'g',         1],
    "decigram": ['metric', 'dg',    0.1],
    "centigram": ['metric', 'cg',   0.01],
    "milligram": ['metric', 'mg',   0.001],
    "microgram": ['metric', 'ug',   0.000001],
    "nanogram": ['metric', 'ng',    0.000000001],
    "picogram": ['metric', 'pg',    0.000000000001],
    "femtogram": ['metric', 'fg',   0.000000000000001],
    "attogram": ['metric', 'ag',    0.000000000000000001],
    "zeptogram": ['metric', 'zg',   0.000000000000000000001],
    "yoctogram": ['metric', 'yg',   0.000000000000000000000001],

    "grain": ['imperial', 'gr', 0.0647989],
    "dram": ['imperial', 'dr', 1.7718452],
    "ounce": ['imperial', 'oz', 28.3495],
    "pound": ['imperial', 'lb', 453.592],
    "stone": ['imperial', 'st', 6350.29],
    "ton": ['imperial', 'ton', 907185],
}
MASS_IMPERIAL = {
    "grain": ['imperial', 'gr', 0.000142857],
    "dram": ['imperial', 'dr', 0.00390625],
    "ounce": ['imperial', 'oz', 0.0625],
    "pound": ['imperial', 'lb', 1],
    "stone": ['imperial', 'st', 14],
    "ton": ['imperial', 'ton', 2000],
    # Metric values were not added to this dictionary since this is only used if 2 imperials are compared
}
VOLUME = {

    "yottaliter": ['metric', 'Yl',   1000000000000000000000000],
    "zettaliter": ['metric', 'Zl',   1000000000000000000000],
    "exaliter": ['metric', 'El',     1000000000000000000],
    "pecaliter": ['metric', 'Pl',     1000000000000000],
    "teraliter": ['metric', 'Tl',     1000000000000],
    "gigaliter": ['metric', 'Gl',     1000000000],
    "megaliter": ['metric', 'Ml',     1000000],
    "kiloliter": ['metric', 'kl',     1000],
    "hectoliter": ['metric', 'hl',    100],
    "decaliter": ['metric', 'dal',    10],
    "liter": ['metric', 'L',          1],
    "deciliter": ['metric', 'dL',     0.1],
    "centiliter": ['metric', 'cL',    0.01],
    "milliter": ['metric', 'mL',      0.001],
    "microliter": ['metric', 'µL',    0.000001],
    "nanoliter": ['metric', 'nL',     0.000000001],
    "picoliter": ['metric', 'pL',     0.000000000001],
    "femtoliter": ['metric', 'fL',    0.000000000000001],
    "attoliter": ['metric', 'aL',     0.000000000000000001],
    "zeptoliter": ['metric', 'zL',    0.000000000000000000001],
    "yactoliter": ['metric', 'yL',    0.000000000000000000000001],
    "rontoliter": ['metric', 'rL',    0.000000000000000000000000001],
    "quectoliter": ['metric', 'qL',   0.000000000000000000000000000001],



    "cubic yottameter": ['metric', 'Ym3',    1000000000000000000000000000000000],
    "cubic zettameter": ['metric', 'Zm3',    1000000000000000000000000000000],
    "cubic exameter": ['metric', 'Em3',      1000000000000000000000000000],
    "cubic pecameter": ['metric', 'Pm3',     1000000000000000000000000],
    "cubic terameter": ['metric', 'Tm3',     1000000000000000000000],
    "cubic gigameter": ['metric', 'Gm3',     1000000000000000000],
    "cubic megameter": ['metric', 'Mm3',     1000000000000000],
    "cubic kilometer": ['metric', 'km3',     1000000000000],
    "cubic hectometer": ['metric', 'hm3',    1000000000],
    "cubic decameter": ['metric', 'dam3',    1000000],
    "cubic meter": ['metric', 'm3',          1000],
    "cubic decimeter": ['metric', 'dm3',     1],
    "cubic centimeter": ['metric', 'cm3',    0.001],
    "cubic millimeter": ['metric', 'mm3',    0.000001],
    "cubic micrometer": ['metric', 'µm3',    0.000000000000001],
    "cubic nanometer": ['metric', 'nm3',     0.000000000000000000000001],
    "cubic picometer": ['metric', 'pm3',     0.000000000000000000000000000000001],
    "cubic femtometer": ['metric', 'fm3',    0.000000000000000000000000000000000000000001],
    "cubic attometer": ['metric', 'am3',     0.000000000000000000000000000000000000000000000000001],
    "cubic zeptometer": ['metric', 'zm3',    0.000000000000000000000000000000000000000000000000000000000001],
    "cubic yactometer": ['metric', 'ym3',    0.000000000000000000000000000000000000000000000000000000000000000000001],
    "cubic rontometer": ['metric', 'rm3',    0.000000000000000000000000000000000000000000000000000000000000000000000000000001],
    "cubic quectometer": ['metric', 'qm3',   0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000001],


    "fluid ounce": ['imperial', 'imperial fl oz', 0.0284131],
    "US fluid ounce": ['us', 'US fl oz', 0.0295735],

    "gill": ['imperial', 'imperial gi', 0.142065],
    "US gill": ['us', 'US gi', 0.118294],

    "cup": ['imperial', 'imperial cup', 0.284131],
    "US cup": ['us', 'US cup', 0.236588],
    "US legal cup": ['us', 'US legal cup', 0.24],

    "pint": ['imperial', 'imperial pt', 0.568261],
    "US liquid pint": ['us', 'US pt', 0.473176],

    "quart": ['imperial', 'imperial qt', 1.13652],
    "US liquid quart": ['us', 'US qt', 0.946353],

    "gallon": ['imperial', 'imperial gal', 3.78541],
    "US liquid gallon": ['us', 'US gal', 3.78541],

    "tablespoon": ['imperial', 'imperial tbsp', 0.0177582],
    "US tablespoon": ['us', 'US tbsp', 0.0147868],

    "teaspoon": ['imperial', 'imperial tsp', 0.00591939],
    "US teaspoon": ['us', 'US tsp', 0.00492892],

    "cubic foot": ['imperial', 'ft3', 28.3168],

    "cubic inch": ['imperial', 'in3', 0.0163871],

}
VOLUME_IMPERIAL = {
    "fluid ounce": ['imperial', 'fl oz', 1],
    "gill": ['imperial', 'imperial gi', 5],
    "cup": ['imperial', 'imperial cup', 10],
    "pint": ['imperial', 'imperial pt', 20],
    "quart": ['imperial', 'imperial qt', 40],
    "gallon": ['imperial', 'imperial gal', 160],
    "tablespoon": ['imperial', 'imperial tbsp', 0.625],
    "teaspoon": ['imperial', 'imperial tsp', 0.208333],
    "cubic foot": ['imperial', 'ft3', 996.614],
    "cubic inch": ['imperial', 'in3', 0.576744],

}
VOLUME_US = {
    "US fluid ounce": ['us', 'US fl oz', 1],
    "US gill": ['us', 'US gi', 4],
    "US cup": ['us', 'US cup', 8],
    "US legal cup": ['us', 'US legal cup', 8.11537],
    "US liquid pint": ['us', 'US pt', 16],
    "US liquid quart": ['us', 'US qt', 32],
    "US liquid gallon": ['us', 'US gal', 128],
    "US tablespoon": ['us', 'US tbsp', 0.5],
    "US teaspoon": ['us', 'US tsp', 0.166667],

    "cubic foot": ['us', 'US ft3', 957.506],
    "cubic inch": ['us', 'US in3', 0.554113],

    "fluid ounce": ['imperial', 'fl oz', 0.96076],
    "gill": ['imperial', 'imperial gi', 4.8038],
    "cup": ['imperial', 'imperial cup', 9.6076],
    "pint": ['imperial', 'imperial pt', 19.2152],
    "quart": ['imperial', 'imperial qt', 38.4304],
    "gallon": ['imperial', 'imperial gal', 153.722],
    "tablespoon": ['imperial', 'imperial tbsp', 0.600475],
    "teaspoon": ['imperial', 'imperial tsp', 0.200158],
}
TEMPERATURE = {  # Structure is different here, since each is its own unit system. The second item is still abbreviation, but the third is for display. Factors don't exist here as a base unit would not be ideal; instead the Converter class handles it differently.
    "celsius": ['temperature', 'C', '°C'],
    "fahrenheit": ['temperature', 'F', '°F'],
    "kelvin": ['temperature', 'K', 'K'],
}
TIME = {
    "millisecond": ['time', 'ms', 0.001],
    "microsecond": ['time', 'us', 0.000001],
    "nanosecond": ['time', 'ns', 0.000000001],

    "minute": ['time', 'min', 60],
    "hour": ['time', 'hr', 3_600],
    "day": ['time', 'days', 86_400],
    "week": ['time', 'weeks', 604_800],
    "fortnight": ['time', 'fortnights', 1_209_600],
    "second": ['time', 'sec', 1],

    "year": ['time', 'years', 31_536_000],
    "non-leap year": ['time', 'non-leap years', 31_536_000],
    "leap year": ['time', 'leap years', 31622400],

    "month": ['time', 'months', ],
    "decade": ['time', 'decades', ],
    "century": ['time', 'centuries', ],
    "millennium": ['time', 'millennia', ],
}

TIME_LARGE = {
    "month": ['time', 'months', 1],
    "year": ['time', 'years', 12],
    "decade": ['time', 'decades', 120],
    "century": ['time', 'centuries', 1200],
    "millennium": ['time', 'millennia', 12000],
}
ALL_DIMENSIONS = ["length", "mass", "volume", "temperature", "time"]
PAIRS = {
    "length": [LENGTH, LENGTH_IMPERIAL],
    "mass": [MASS, MASS_IMPERIAL],
    "volume": [VOLUME, VOLUME_IMPERIAL, VOLUME_US],
    "temperature": [TEMPERATURE],
    "time": [TIME]
}

# Variables used later on:
# data = variable for instance of Data
# converter = variable for instance of Converter

# Handle user input


class Input:
    def new_input(userinput):
        try:
            answer = input(userinput).strip()
            # User wants to see available units
            if answer == "units":
                Input.show_units()
            # User wants to exit
            elif answer == "exit":
                print("\nExiting.")
                exit()
            # If answer passed above tests,
            # return it just like input()
            else:
                return answer
        # Also handles KeyboardInterrupt
        except KeyboardInterrupt:
            print("\nExiting.")
            exit()

    def show_units():
        print("AVAILABLE UNITS:")
        for key, value in data.units.items():
            print(key + " (" + value[1] + ")")

# Collect and validate data, no calculations


class Data:
    def __init__(self):
        # Just to see all self vars, not required to declare them first obviously
        # Type of measurement
        self.measurement_type = None
        # used to shorten typing PAIRS[self.measurement_type][0] within the Data class,
        # the dictionary being used from PAIRS. It just tells this class what all the units are
        self.units = None
        # Starting unit
        self.unit_to_convert = None
        # Ending unit
        self.unit_to_convert_to = None
        # Value to convert
        self.value_to_convert = None

        # Get measurement type
        self.measurement_type = Input.new_input(
            "\n\nEnter the measurement type (length, mass, volume, temperature, time): "
        )
        while self.measurement_type not in ALL_DIMENSIONS:
            self.measurement_type = input(
                "\nInvalid measurement type.\nEnter the measurement type (length, mass, volume, temperature): "
            )

        if self.measurement_type == "volume":
            print("WARNING: FOR VOLUME, IMPERIAL UNITS ARE DIFFERENT FROM US UNITS.\nTYPE 'units' TO SEE ALL UNITS.")

        if self.measurement_type == "time":
            print("WARNING: When converting certain values to years, the converter does not take into account the current year. It assumes that every 4 years is a leap year.")
        # Program starts out with metric just to see all of the units
        # (metric will always have the most since imperial dicts only contain imperials, while metrics contain both)
        self.units = PAIRS[self.measurement_type][0]

    def check_unit(self, string):
        if not (string in self.units or any(string == lst[1] for lst in self.units.values())):
            return False, False
        else:
            if any(string in lst[1] for lst in self.units.values()):
                return True, True
            else:
                return True, False

    def get_data(self):
        # Get unit_to_convert
        unit_to_convert = Input.new_input(
            "\nEnter the unit (e.g. meter or m) you'd like to convert: "
        )
        while not self.check_unit(unit_to_convert)[0]:
            unit_to_convert = Input.new_input(
                "\nInvalid unit.\nEnter the unit (e.g. meter or m) you'd like to convert: "
            )
        # If the input is a second item in any of the lists (abbreviation), change it to the key.
        if self.check_unit(unit_to_convert)[1]:
            for key, value in self.units.items():     # For every key and value in the dictionary
                if value[1] == unit_to_convert:  # check if the input was the second item
                    unit_to_convert = key        # change the input to the key
                    break                        # break out of the loop

        self.unit_to_convert = unit_to_convert
        print()  # need to print a new line because of the structure of the get_value_to_convert() method

        # Get value_to_convert
        while True:
            try:  # Try to successfully capture the user's input as a float
                self.value_to_convert = float(
                    Input.new_input("Enter the value you'd like to convert: "))
                break  # If successful, break out of the loop
            except ValueError:  # If they entered an invalid value that could not satisfy the float() method, retry
                print("\nInvalid value. Please try again.")

        # Get the unit to convert the value to (unit_to_convert_to)
        self.unit_to_convert_to = Input.new_input(
            "\nEnter the unit you'd like to convert it to: ")

        # While input is not a spelled out unit or is not the second item in any of the lists, input again.
        while not self.check_unit(self.unit_to_convert_to)[0]:
            self.unit_to_convert_to = Input.new_input(
                "\nInvalid unit.\nEnter the unit (e.g. meter or m) you'd like to convert: ")

        # If the input is a second item in any of the lists, change it to the key.
        # In-depth: For every key and value in the dictionary, check if input was second item, change the input to the key, break out of loop
        if self.check_unit(self.unit_to_convert_to)[1]:
            for key, value in self.units.items():
                if value[1] == self.unit_to_convert_to:
                    self.unit_to_convert_to = key
                    break

    # Used to be used, but got deprecated
    # def return_values(self):
    #     return self.unit_to_convert, self.value_to_convert, self.unit_to_convert_to, self.measurement_type


class Converter:
    def __init__(self):
        self.unit_to_convert = data.unit_to_convert
        self.value_to_convert = data.value_to_convert
        self.unit_to_convert_to = data.unit_to_convert_to
        self.measurement_type = data.measurement_type
        self.units = data.units

    def set_base(self):
        # if the measurement systems are the same, set the dict variable to the matching system.

        # If they're both metric
        if self.units[self.unit_to_convert][0] == self.units[self.unit_to_convert_to][0] == "metric":
            self.units = PAIRS[self.measurement_type][0]
        # If they're both imperial
        elif self.units[self.unit_to_convert][0] == self.units[self.unit_to_convert_to][0] == "imperial":
            self.units = PAIRS[self.measurement_type][1]
        # If they're both US
        # Only for volume, since us has different ones
        elif self.units[self.unit_to_convert][0] == self.units[self.unit_to_convert_to][0] == "us":
            self.units = PAIRS[self.measurement_type][2]
        # If one is imperial and one is US
        # Only for volume, since us has different ones
        elif (self.units[self.unit_to_convert][0] == "imperial" and self.units[self.unit_to_convert_to][0] == "us") or (self.units[self.unit_to_convert][0] == "us" and self.units[self.unit_to_convert_to][0] == "imperial"):
            self.units = PAIRS[self.measurement_type][2]
            print(
                f"WARNING: Conversion from {self.units[self.unit_to_convert][0]} to {self.units[self.unit_to_convert_to][0]} is an approximation, since they are from different measurement systems.")
        elif self.units[self.unit_to_convert][0] == self.units[self.unit_to_convert_to][0] == "temperature":
            pass  # self.units is already temperature, and nothing to print

        elif self.units[self.unit_to_convert][0] == self.units[self.unit_to_convert_to][0] == "time":
            # We want to have leap years to be seen by setting years to years
            if (self.unit_to_convert in TIME_LARGE and self.unit_to_convert_to in TIME_LARGE):
                self.units = TIME_LARGE
            if self.unit_to_convert == self.unit_to_convert_to == 'year':
                self.units = TIME
            elif self.units[self.unit_to_convert] == "month" or self.units[self.unit_to_convert_to] == "month":
                print("WARNING: Conversion between these 2 units is VERY approximate.")
                self.units = TIME

        # Anything else
        else:
            print(
                f"WARNING: Conversion from {self.units[self.unit_to_convert][0]} to {self.units[self.unit_to_convert_to][0]} is an approximation, since they are from different measurement systems or aren't exact.")
            self.units = PAIRS[self.measurement_type][0]

    def convert(self, dict):
        if self.measurement_type in ['length', 'mass', 'volume', 'time']:
            if self.measurement_type == 'time':
                # user is converting a year into something else (with leap years included)
                if self.units != TIME_LARGE:
                    if self.unit_to_convert == 'year' and self.unit_to_convert_to != 'year':
                        # User wants to convert years to something
                        print(
                            "WARNING: Calculating the amount of leap years is sometimes an approximation, depending on how large the number is.")
                        # self.value_to_convert is the number of years
                        # calculate the number of leap years with this formula
                        leap_years = (self.value_to_convert // 4) - \
                            (self.value_to_convert // 100) + \
                            (self.value_to_convert // 400)
                        # calculate the number of seconds that will be added (1 leap year is 1 day, which is 86400 seconds)
                        days_in_seconds_added = leap_years * 86400
                        # originally for debug but is good so the user knows that leap years were taken into account
                        print(
                            f"Leap years: {leap_years}, Seconds of leap years: {days_in_seconds_added}")
                        # Basically use the same formula as other units but add extra seconds
                        # return    |------------ value in seconds ------------------|   +   added days in seconds /           factor
                        return ((self.value_to_convert * dict[self.unit_to_convert][2]) + days_in_seconds_added) / dict[self.unit_to_convert_to][2]

                    elif self.unit_to_convert_to == 'year' and self.unit_to_convert != 'year':
                        # User wants to convert something to years
                        seconds_without_leap_years = self.value_to_convert * \
                            dict[self.unit_to_convert][2]
                        converted_to_years = seconds_without_leap_years / 31_536_000
                        leap_years = (converted_to_years // 4) - \
                            (converted_to_years // 100) + \
                            (converted_to_years // 400)
                        # calculate the number of seconds that will be added (1 leap year is 1 day, which is 86400 seconds)
                        days_in_seconds_added = leap_years * 86400
                        # originally for debug but is good so the user knows that leap years were taken into account
                        print(
                            f"Leap years: {leap_years}, Seconds of leap years: {days_in_seconds_added}")
                        # Basically use the same formula as other units but add extra seconds
                        # With leap years, without leap years:
                        return str((seconds_without_leap_years - days_in_seconds_added) / dict[self.unit_to_convert_to][2]) + " years, including leap", str(self.value_to_convert * dict[self.unit_to_convert][2] / dict[self.unit_to_convert_to][2]) + " 365-day"

                    elif self.unit_to_convert == 'decade' and self.unit_to_convert_to != 'decade':
                        # User wants to convert decades to something
                        print(
                            "WARNING: Calculating the amount of leap years is sometimes an approximation, depending on how large the number is.")
                        years = self.value_to_convert * 10
                        # self.value_to_convert is the number of years
                        # calculate the number of leap years with this formula
                        leap_years = (years // 4) - \
                            (years // 100) + \
                            (years // 400)
                        # calculate the number of seconds that will be added (1 leap year is 1 day, which is 86400 seconds)
                        days_in_seconds_added = leap_years * 86400
                        # originally for debug but is good so the user knows that leap years were taken into account
                        print(
                            f"Leap years: {leap_years}, Seconds of leap years: {days_in_seconds_added}")
                        return (((years * dict["year"][2]) + days_in_seconds_added) / 10) / dict[self.unit_to_convert_to][2]

                    elif self.unit_to_convert_to == 'decade' and self.unit_to_convert != 'decade':
                        # User wants to convert something to decades
                        print(
                            "\nNOTE: The result does not include leap years. 365-day years are used.")
                        # TODO: Make it available in all 3 types of years

                    elif self.unit_to_convert == 'century' and self.unit_to_convert_to != 'century':
                        print(
                            "WARNING: Calculating the amount of leap years is sometimes an approximation, depending on how large the number is.")
                        years = self.value_to_convert * 100
                        # self.value_to_convert is the number of years
                        # calculate the number of leap years with this formula
                        leap_years = (years // 4) - \
                            (years // 100) + \
                            (years // 400)
                        # calculate the number of seconds that will be added (1 leap year is 1 day, which is 86400 seconds)
                        days_in_seconds_added = leap_years * 86400
                        # originally for debug but is good so the user knows that leap years were taken into account
                        print(
                            f"Leap years: {leap_years}, Seconds of leap years: {days_in_seconds_added}")
                        return (((years * dict["year"][2]) + days_in_seconds_added) / 100) / dict[self.unit_to_convert_to][2]

                    elif self.unit_to_convert_to == 'century' and self.unit_to_convert != 'century':
                        print(
                            "\nNOTE: The result does not include leap years. 365-day years are used.")
                        # TODO: Make it available in all 3 types of years

            return (self.value_to_convert * dict[self.unit_to_convert][2]) / dict[self.unit_to_convert_to][2]

        elif self.measurement_type == 'temperature':

            if self.unit_to_convert == 'celsius' and self.unit_to_convert_to == 'fahrenheit':
                return (self.value_to_convert * 9 / 5) + 32

            elif self.unit_to_convert == 'fahrenheit' and self.unit_to_convert_to == 'celsius':
                return (self.value_to_convert - 32) * 5 / 9

            elif self.unit_to_convert == 'kelvin' and self.unit_to_convert_to == 'celsius':
                return self.value_to_convert - 273.15

            elif self.unit_to_convert == 'celsius' and self.unit_to_convert_to == 'kelvin':
                return self.value_to_convert + 273.15

            elif self.unit_to_convert == 'kelvin' and self.unit_to_convert_to == 'fahrenheit':
                return (self.value_to_convert - 273.15) * 9 / 5 + 32

            elif self.unit_to_convert == 'fahrenheit' and self.unit_to_convert_to == 'kelvin':
                return (self.value_to_convert - 32) * 5 / 9 + 273.15

    def print_output(self):
        if self.measurement_type in ['length', 'mass', 'volume', 'time']:
            # try will work if the function returns 2 values (when the answer is shown with and without leap years included)
            try:
                for item in self.convert(self.units):
                    print(
                        f"\n\n{self.value_to_convert} {self.units[self.unit_to_convert][1]} is {item} {self.units[self.unit_to_convert_to][1]}")
            except TypeError:
                print(
                    f"\n\n{self.value_to_convert} {self.units[self.unit_to_convert][1]} is {self.convert(self.units)} {self.units[self.unit_to_convert_to][1]}")
        elif self.measurement_type == "temperature":
            print(
                f"\n\n{self.value_to_convert} {self.units[self.unit_to_convert][2]} is {str(self.convert(self.units))} {self.units[self.unit_to_convert_to][2]}")


# Create new metric converter
while True:
    data = Data()
    data.get_data()
    converter = Converter()
    converter.set_base()
    converter.print_output()
